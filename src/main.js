const form = document.getElementById('reg_form');

form.addEventListener('submit', event => {
 
    let p1 = document.getElementById("password").value
    let p2 = document.getElementById("password_repeat").value
    let error_msg_password = document.getElementById("error_password")
if (p1 !== p2){
    
    error_msg_password.innerHTML = "Passwords don't match.";
    event.preventDefault();
    error_msg_password.style.display='block';
}
else if (p1.length<6){
   
    error_msg_password.innerHTML = "Password length must be at least 6 characters";
    event.preventDefault();
    error_msg_password.style.display='block';
} else 
{ error_msg_password.style.display='none';}

let num = document.getElementById("cell_num").value;
let pattern = new RegExp('^[0-9]{8}$');

let isCorrectNum = pattern.test(num);
let error_msg_number = document.getElementById("error_number")
if(!isCorrectNum){
    
    error_msg_number.innerHTML = "Please input in 8-digit format, (e.g., 55123456)";
    event.preventDefault();
    error_msg_number.style.display='block';
}
else 
{ error_msg_number.style.display='none';}


let dob = document.getElementById("dob").value;
let error_msg_dob = document.getElementById("error_dob")
let year = dob.split('-')[0];
let min_year = new Date().getFullYear()-99;
let max_year = new Date().getFullYear()-1;
console.log(min_year,year,max_year,year>max_year||year<min_year);
if(year>max_year||year<min_year){
    
    let msg = "The year should be in range of ".concat(min_year," and ",max_year);
    error_msg_dob.innerHTML = msg;
    event.preventDefault();
    error_msg_dob.style.display='block'
    

}
else {
    error_msg_dob.style.display='none';
}


}



);
